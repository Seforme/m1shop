<?php


class Db
{

    protected $_config;
    public $dbc;


    public function __construct() {

        $this->_config = new Config();
        $this->getPDOConnection();
    }

    public function __destruct() {
        $this->dbc = NULL;
    }

    private function getPDOConnection() {
        // Check if the connection is already established
        if ($this->dbc == NULL) {
            //create new connection
            $dsn = "" .
                $this->_config->db['driver'] .
                ":host=" . $this->_config->db['host'] .
                ";dbname=" . $this->_config->db['dbname'] .
                ";charset=utf8";
            try {
                $this->dbc = new PDO( $dsn, $this->_config->db[ 'username' ], $this->_config->db[ 'password' ] );
            } catch( PDOException $e ) {
                if($e->getCode() == 1049){

                    return $this->migrate();
                }else{
                    echo __LINE__.$e->getMessage();
                }

            }
        }
    }

    public function execute( $sql ) {
        try {
            $count = $this->dbc->exec($sql) or print_r($this->dbc->errorInfo());
        } catch(PDOException $e) {
            echo __LINE__.$e->getMessage();
        }
        return $count;
    }


	public function getAssoc( $sql ) {
        $stmt = $this->dbc->prepare( $sql );
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $result = $stmt->fetchAll();
        return $result;
    }

    public function getOne( $sql ) {
        $stmt = $this->dbc->prepare( $sql . ' LIMIT 1' );
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $result = $stmt->fetchAll();
        if(count($result)){
            return $result[0];
        }
        return [];
    }

    public function run( $sql ) {
        try {
            $count = $this->dbc->exec($sql) or print_r($this->dbc->errorInfo());
        } catch(PDOException $e) {
            echo __LINE__.$e->getMessage();
        }
        return $count;
    }

    private function migrate(){
        if ($this->dbc == NULL) {
            // Create the connection
            $dsn = "" .
                $this->_config->db['driver'] .
                ":host=" . $this->_config->db['host'];
            try {
                $this->dbс = new PDO( $dsn, $this->_config->db[ 'username' ], $this->_config->db[ 'password' ] );
            } catch( PDOException $e ) {
                    echo __LINE__.$e->getMessage();
            }
        }
        $this->dbс->exec("CREATE DATABASE `" . $this->_config->db['dbname'] . "`; USE `" . $this->_config->db['dbname'] . "`;")
        or die(print_r($this->dbс->errorInfo(), true));
        $automigrate = file_get_contents(MIGRATE . '/automigrate.sql');
        $this->dbс->exec($automigrate);

    }
}