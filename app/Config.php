<?php

class Config
{
    public function __construct() {
        $this->db = array(
            'driver' => 'mysql',
            'host' => 'localhost',
            'dbname' => 'm1shop',
            'username' => 'root',
            'password' => ''
        );
        $this->pagination = array(
            'limit' => 10,
        );
        $this->allowedGetValue = array(
            'ASC','DESC','username','email','status'
        );

    }
}