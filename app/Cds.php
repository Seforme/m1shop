<?php


class Cds
{
    protected $_config, $_db;

    public function __construct() {

        $this->_config = new Config();
        $this->_db = new Db();

    }
    public function getAllCds($where = ''){

        return $this->_db->getAssoc('SELECT * FROM cds '.$where);
    }

    public function getValue($var,$default=''){
        if(isset($_POST) && isset($_POST[$var])){
            return $this->clear($_POST[$var]);
        }
        else return $default;
    }

    public function addCds($name,$artist,$year,$duration,$date,$cost,$code){

        $this->_db->run('INSERT INTO cds (`name`,`artist`,`year`,`duration`,`date`,`cost`,`code`) 
                                          VALUES("' . $name . '","' . $artist . '","' . $year . '","' . $duration . '","' . $date . '","' . $cost . '","' . $code . '");');

        return  $this->_db->dbc->lastInsertId('cds');


    }

    public function deleteCds($id){

        return $this->_db->run('DELETE FROM cds WHERE `id`='. $id .';');




    }


    public function updateCds($id,$name,$artist,$year,$duration,$date,$cost,$code){
        return $this->_db->run('UPDATE cds SET `name` = "' . $name . '",`artist`="' . $artist . '",`year`="' . $year . '",`duration`=' . $duration . ',`date`="' . $date . '",`cost`=' . $cost . ',`code`="' . $code . '" WHERE `id`='.$id);
    }



    public function clear($msg)
    {
        $msg = stripslashes($msg);
        $msg = htmlspecialchars(trim($msg));
        //$msg = mysql_real_escape_string($msg);
        $msg = str_replace("`", "", $msg);
        $msg = str_replace("%", "", $msg);
        $msg = str_replace("../", "", $msg);
        $msg = str_replace("\0", "", $msg);

        return $msg;
    }


}