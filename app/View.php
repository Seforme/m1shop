<?php
class View
{
    //public $template_view; // здесь можно указать общий вид по умолчанию.

    function load($template_view, $data = null)
    {

        if(is_array($data)) {
            extract($data);
        }


        include V.'/'.$template_view.'.php';
    }
}