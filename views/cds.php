<?php
include V . '/common/header.php';
?>
    <div class="container pt-4">
        <button type="button" class="btn btn-lg btn-block btn-success mb-4" onclick="$('#newtaskform').toggle();">Add
            new CD
        </button><?php
        if(isset($_GET['artist'])){?>
            <a href="/" class="btn btn-lg btn-block btn-success mb-4">Очистить фильтр</a>

        <?php } ?>
        <div class="card mb-4" style="display: none" id="newtaskform">
            <div class="card-header">
                <h4 class="my-0 font-weight-normal">Add new CD</h4>
            </div>
            <div class="card-body">
                <form method="POST" id="formadd" enctype="multipart/form-data">
                    <div class="input-group mb-3">
                        <input type="text" name="name" class="form-control" placeholder="Название альбома">
                    </div>
                    <div class="input-group mb-3">
                        <input type="text" name="artist" class="form-control" placeholder="Исполнитель">
                    </div>
                    <div class="input-group mb-3">
                        <input type="number" name="year" class="form-control" placeholder="Год выпуска">
                    </div>
                    <div class="input-group mb-3">
                        <input type="number" name="duration" class="form-control" placeholder="Продолжительность">
                    </div>
                    <div class="input-group mb-3">
                        <input type="date" name="date" class="form-control" placeholder="Дата покупки">
                    </div>
                    <div class="input-group mb-3">
                        <input type="number" name="cost" class="form-control" placeholder="Стоимость">
                    </div>
                    <div class="input-group mb-3">
                        <input type="text" name="code" class="form-control" placeholder="Код хранилища">
                    </div>
                    <div class="input-group mb-3">
                        <input type="file" name="image" class="form-control" placeholder="Обложка альбома">
                    </div>
                    <button type="submit" class="btn btn-lg btn-block btn-success mt-4">Сохранить</button>
                </form>


            </div>
        </div>
        <?php
        if(isset($_GET['artist'])){
            $artist = '&artist='.$_GET['artist'];
        }else{
            $artist='';
        }
        ?>
        <div class="d-flex justify-content-around">
            <a href="?sortby=year&n=ASC<?=$artist?>">Sort By: year ↑</a><a href="?sortby=year&n=DESC<?=$artist?>">Sort By: year ↓</a>
            <a href="?sortby=name&n=ASC<?=$artist?>">Sort By: name ↑</a><a href="?sortby=name&n=DESC<?=$artist?>">Sort By: name ↓</a>
            <a href="?sortby=duration&n=ASC<?=$artist?>">Sort By: duration ↑</a><a href="?sortby=duration&n=DESC<?=$artist?>">Sort By: duration ↓</a>
        </div>
        <?php foreach ($cds as $cd) { ?>
            <div class="card mb-4 shadow-sm">
                <div class="card-header">
                    <h4 class="my-0 font-weight-normal"><?= $cd['name'] ?></h4>
                </div>
                <div class="card-body row">
                    <div class="md-4 float-left mr-1">
                        <img src="images/<?= $cd['id'] ?>.jpg">
                    </div>
                    <div class="md-4 mr-1">
                        <p>Исполнитель: <a href="?artist=<?= $cd['artist'] ?>"><?= $cd['artist'] ?></a></p>
                        <p>Год выпуска: <?= $cd['year'] ?></p>
                        <p>Продолжительность: <?= $cd['duration'] ?> мин.</p>
                    </div>
                    <div class="md-4">
                        <p>Дата покупки: <?= $cd['date'] ?></p>
                        <p>Стоимость: <?= $cd['cost'] ?></p>
                        <p>Код хранилища: <?= $cd['code'] ?></p>
                    </div>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#updateModal"
                            data-id="<?= $cd['id'] ?>" data-name="<?= $cd['name'] ?>" data-artist="<?= $cd['artist'] ?>"
                            data-year="<?= $cd['year'] ?>" data-duration="<?= $cd['duration'] ?>"
                            data-date="<?= $cd['date'] ?>"
                            data-cost="<?= $cd['cost'] ?>" data-code="<?= $cd['code'] ?>">Редактировать
                    </button>

                    <form method="POST">
                        <input type="hidden" name="delete" value="1">
                        <input type="hidden" name="id" value="<?= $cd['id'] ?>">
                        <button type="submit" class="btn btn-danger">удалить
                        </button>
                    </form>
                </div>
            </div>
        <?php } ?>


    </div>
    <div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="updateModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="updateModalLabel">Редактировать</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" enctype="multipart/form-data">
                    <div class="modal-body">

                        <div class="input-group mb-3">
                            <input type="hidden" name="id" id="modal-id">
                            <input type="text" name="name" id="modal-name" class="form-control"
                                   placeholder="Название альбома">
                        </div>
                        <div class="input-group mb-3">
                            <input type="text" name="artist" id="modal-artist" class="form-control"
                                   placeholder="Исполнитель">
                        </div>
                        <div class="input-group mb-3">
                            <input type="number" name="year" id="modal-year" class="form-control"
                                   placeholder="Год выпуска">
                        </div>
                        <div class="input-group mb-3">
                            <input type="number" name="duration" id="modal-duration" class="form-control"
                                   placeholder="Продолжительность">
                        </div>
                        <div class="input-group mb-3">
                            <input type="date" name="date" id="modal-date" class="form-control"
                                   placeholder="Дата покупки">
                        </div>
                        <div class="input-group mb-3">
                            <input type="number" name="cost" id="modal-cost" class="form-control"
                                   placeholder="Стоимость">
                        </div>
                        <div class="input-group mb-3">
                            <input type="text" name="code" id="modal-code" class="form-control"
                                   placeholder="Код хранилища">
                        </div>
                        <div class="input-group mb-3">
                            <input type="file" name="image" class="form-control" placeholder="Обложка альбома">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Send message</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>
        window.onload = function () {
            $('#updateModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                var id = button.data('id');
                var name = button.data('name');
                var artist = button.data('artist');
                var year = button.data('year');
                var duration = button.data('duration');
                var date = button.data('date');
                var cost = button.data('cost');
                var code = button.data('code');

                var modal = $(this);
                modal.find('#modal-id').val(id);
                modal.find('#modal-name').val(name);
                modal.find('#modal-artist').val(artist);
                modal.find('#modal-year').val(year);
                modal.find('#modal-duration').val(duration);
                modal.find('#modal-date').val(date);
                modal.find('#modal-cost').val(cost);
                modal.find('#modal-code').val(code);
            })
        }
    </script>


<?php
include V . '/common/footer.php';