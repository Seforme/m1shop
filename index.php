<?php
require_once "_autoload.php";
$cds = new Cds();
$view = new View();
$data['title']='My CD\'s';
if(isset($_POST) && $_POST && !isset($_POST['delete'])){
    $id = $cds->getValue('id', null);


    $name = $cds->getValue('name');
    $artist = $cds->getValue('artist');
    $year = $cds->getValue('year');
    $duration = $cds->getValue('duration');
    $date = $cds->getValue('date');
    $cost = $cds->getValue('cost');
    $code = $cds->getValue('code');
    if($id === null){
        $id = $cds->addCds($name,$artist,$year,$duration,$date,$cost,$code);
    }else{
        $cds->updateCds($id,$name,$artist,$year,$duration,$date,$cost,$code);
    }

    if(isset($_POST['image'])) {
        $imageSize = getimagesize($_FILES['image']['tmp_name']);
        if ($imageSize !== false) {
            move_uploaded_file($_FILES['image']['tmp_name'], IMG . '/' . $id . '.jpg');
        }
    }
}elseif (isset($_POST['delete']) && $_POST['delete']==1){
    $cds->deleteCds($_POST['id']);
}

if(isset($_GET['sortby']) && isset($_GET['n'])){
    $order = " ORDER BY ".$cds->clear($_GET['sortby']). " ".$cds->clear($_GET['n']);
}else{
    $order='';
}

if(isset($_GET['artist'])){
    $data['cds'] = $cds->getAllCds('WHERE artist="'.$cds->clear($_GET['artist']).'"'.$order);
}else{
    $data['cds'] = $cds->getAllCds();
}




$view->load('cds',$data);
