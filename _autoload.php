<?php
define('DIR', __DIR__);
define('APP', DIR . '/app');
define('V', DIR . '/views');
define('MIGRATE', DIR . '/migrate');
define('IMG', DIR . '/images');

spl_autoload_register(function ($class){
    include APP . '/' . $class . '.php';
});